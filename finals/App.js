import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import Swiper from 'react-native-swiper/src';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

function Cover ({navigation}){
  return(
    <View style={{
     flex: 1,
     justifyContent: 'center', 
     alignItems: 'center',
     selfAlign: 'stretch',
     textAlign: 'center', backgroundColor: 'darkblue'}}>
    <Image
     style={{height: 350, width: 340}}
     source={require('./images/Cover.jpg')}
    />
    <TouchableOpacity style={{
     alignItems: "center",
     backgroundColor: "skyblue",
     padding: 5, paddingLeft: 50, paddingRight: 50,
     borderRadius: 15}} onPress={() => navigation.navigate('list')}>
    <Text style = {{fontSize: 20, fontWeight: 'bold'}}>Open</Text>
    </TouchableOpacity>
    </View>)
}
function List ({navigation}){
  return(
    <View style={{
     flex: 1,
     justifyContent: 'center', 
     alignItems: 'center',
     selfAlign: 'stretch',
     textAlign: 'center', backgroundColor: 'white'}}>
    <Text style={{fontSize: 27, fontWeight: 'bold'}} >Table of Contents</Text>
     <TouchableOpacity style={{
     alignItems: "center",
     backgroundColor: "yellow",
     padding: 5, paddingLeft: 50, paddingRight: 50,
     borderRadius: 15}} onPress={() => navigation.navigate('donation')}>
    <Text style = {{fontSize: 20}}>Donation drive</Text>
    </TouchableOpacity>
    <TouchableOpacity style={{
     alignItems: "center",
     backgroundColor: "lightblue",
     padding: 5, paddingLeft: 50, paddingRight: 50,
     borderRadius: 15}} onPress={() => navigation.navigate('classes')}>
    <Text style = {{fontSize: 20}}>Online Classes</Text>
    </TouchableOpacity>
    </View>)
}
function Donation ({navigation}){
  return (
    <Swiper style = {styles.wrapper} showsButtons={true}
     prevButton={<Text style={{fontWeight:'bold'}}></Text>}>
    <View style={styles.slide1}>
    <Image 
     style={{height: 300 , width: 300}}
     source={require('./images/Donation.jpg')}
    /> 
    </View>
    <View style={styles.slide2}>
    <Text style={styles.heading} >Purchasing of School Supplies</Text>
    <Text style={styles.heading2} >May 17, 2022 @Superstar</Text>
    <Image
     style={{height: 100, width: 200}}
     source={require('./images/1Purchasing051722.jpg')}
    />
    <Image
     style={{height: 100, width: 200}}
     source={require('./images/2Purchasing051722.jpg')}
    />
    <Image
     style={{height: 100, width: 200}}
     source={require('./images/3Purchasing051722.jpg')}
    />
    </View>
    <View style={styles.slide3}>
    <Text style={styles.heading} >Packing of School Supplies</Text>
    <Text style={styles.heading2} >May 17, 2022 @Rosada's Residence</Text>
    <Image
     style={{height: 100, width: 200}}
     source={require('./images/1Packing051722.jpg')}
    />
    <Image
     style={{height: 100, width: 200}}
     source={require('./images/2Packing051722.jpg')}
    />
    <Image
     style={{height: 100, width: 200}}
     source={require('./images/3Packing051722.jpg')}
    />
    </View>
    <View style={styles.slide4}>
    <Text style={styles.heading} >Donation Day</Text>
    <Text style={styles.heading2} >May 18, 2022 @Greendale RIS</Text>
    <Image
     style={{height: 100, width: 200}}
     source={require('./images/1Donating051822.jpg')}
    />
    <Image
     style={{height: 100, width: 200}}
     source={require('./images/2Donating051822.jpg')}
    />
    <Image
     style={{height: 100, width: 200}}
     source={require('./images/3Donating051822.jpg')}
    />
    <TouchableOpacity style={{
     alignItems: "center",
     backgroundColor: "white",
     padding: 2, paddingLeft: 10, paddingRight: 10,
     borderRadius: 5}} onPress={() => navigation.navigate('cover')}>
    <Text style = {{fontSize: 16, fontWeight: 'bold'}}>Close</Text>
    </TouchableOpacity>
    <TouchableOpacity style={{
     alignItems: "center",
     backgroundColor: "lightblue",
     padding: 2, paddingLeft: 10, paddingRight: 10,
     borderRadius: 5}} onPress={() => navigation.navigate('list')}>
    <Text style = {{fontSize: 16, fontWeight: 'bold'}}>Back</Text>
    </TouchableOpacity>
    </View>
    </Swiper>);
}
function Classes ({navigation}){
  return (
    <Swiper style = {styles.wrapper} showsButtons={true}
     prevButton={<Text style={{fontWeight:'bold'}}></Text>}>
    <View style={styles.slide1}>
    <Image 
     style={{height: 200, width: 400}}
     source={require('./images/OnlineClasses.jpg')}
    /> 
    </View>
    <View style={styles.slide2}>
    <Text style={styles.heading} >Online Classes of BSIT Freshmen Students</Text>
    <Text style={styles.heading2} >S.Y. 2021-2022</Text>
    <Image
     style={{height: 100, width: 200}}
     source={require('./images/ProgrammingIIJavaClass.jpg')}
    />
    <Image
     style={{height: 100, width: 200}}
     source={require('./images/MobileDevelopmentClass.png')}
    />
    <Image
     style={{height: 100, width: 200}}
     source={require('./images/ComputingComputerClass.jpg')}
    />
    <TouchableOpacity style={{
     alignItems: "center",
     backgroundColor: "white",
     padding: 2, paddingLeft: 10, paddingRight: 10,
     borderRadius: 5}} onPress={() => navigation.navigate('cover')}>
    <Text style = {{fontSize: 16, fontWeight: 'bold'}}>Close</Text>
    </TouchableOpacity>
    <TouchableOpacity style={{
     alignItems: "center",
     backgroundColor: "lightblue",
     padding: 2, paddingLeft: 10, paddingRight: 10,
     borderRadius: 5}} onPress={() => navigation.navigate('list')}>
    <Text style = {{fontSize: 16, fontWeight: 'bold'}}>Back</Text>
    </TouchableOpacity>
    </View>
    </Swiper>);
}
const Stack = createNativeStackNavigator();

export default function App () {
  return(
    <NavigationContainer>
    <Stack.Navigator>
    <Stack.Screen name= 'cover' component={Cover} options={{headerShown: false}} />
    <Stack.Screen name= 'donation' component={Donation} options={{headerShown: false}} />
    <Stack.Screen name= 'list' component={List} options={{headerShown: false}} />
    <Stack.Screen name= 'classes' component={Classes} options={{headerShown: false}} />
    </Stack.Navigator>
    </NavigationContainer>);
}
const styles = StyleSheet.create({

  wrapper:{},
  slide1:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    textAlign: 'center',
    backgroundColor: 'yellow'
  },
  slide2:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'stretch',
    textAlign: 'center',
    backgroundColor: 'yellow'
  },
  slide3:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    backgroundColor: 'yellow'
  },
  slide4:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    backgroundColor: 'yellow'
  },
  heading:{
    fontSize: 24,
    fontWeight: 'bold',
    paddingVertical: 10,
    textAlign: 'center',
    backgroundColor: 'yellow'
  },
  heading2:{
    fontSize: 20,
    paddingVertical: 10,
    textAlign: 'center',
    backgroundColor: 'yellow'
  },
})

