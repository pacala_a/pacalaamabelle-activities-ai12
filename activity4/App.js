import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Swiper from 'react-native-swiper'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

function firstScreen ({ navigation }) {
  return (
    <View style={{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center', backgroundColor: 'white'}}>
      <Image
      style={{height: 100, width: 200}}
      source={require('./images/undrawThings.png')}
      />
      <Text style={styles.heading} >Things that Describe Why I Chose IT Course</Text>
      <TouchableOpacity style={{
        alignItems: "center",
        backgroundColor: "#DDDDDD",
        padding: 10, paddingLeft: 20, paddingRight: 20,
        borderRadius: 20}} onPress={() => navigation.navigate('second')}>
      <Text style = {{fontSize: 20}}>Continue</Text>
      </TouchableOpacity>
    </View>
  )
}

function secondScreen ({ navigation }) {
  return (
    <Swiper style = {styles.wrapper} showsButtons={true}
    prevButton={<Text style={{fontWeight:'bold'}}></Text>}>
    <View style={styles.slide2}>
       <Image 
      style={{height: 100, width: 200}}
      source={require('./images/undrawOnline.png')}
      />
      <Text style={styles.heading2} > IT course will surely help me learn computer</Text>
    </View>
    <View style={styles.slide3}>
       <Image
      style={{height: 100, width: 200}}
      source={require('./images/undrawWeb.png')}
      />
      <Text style={styles.heading2} > Computer and the stuffs in it got my interest</Text>
    </View>
    <View style={styles.slide4}>
       <Image
      style={{height: 100, width: 200}}
      source={require('./images/undrawMobile.png')}
      />
      <Text style={styles.heading2} > It is amusing to learn how technology works</Text>
    </View>
    <View style={styles.slide5}>
       <Image
      style={{height: 100, width: 200}}
      source={require('./images/undrawWIP.png')}
      />
      <Text style={styles.heading2} > I want to work on becoming an information technologist</Text>
      <TouchableOpacity style={{
        alignItems: "center",
        backgroundColor: "#DDDDDD",
        padding: 10, paddingLeft: 20, paddingRight: 20,
        borderRadius: 20}} onPress={() => navigation.navigate('first')}>
      <Text style = {{fontSize: 20}}>Home</Text>
      </TouchableOpacity>
    </View>
  </Swiper>
    );
}
  const styles = StyleSheet.create({

  wrapper: {},
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    backgroundColor: 'white'
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    backgroundColor: 'white'
  },
  slide4: {
    flex: 1,
    justifyContent: 'center',
     alignItems: 'center',
     textAlign: 'center',
     backgroundColor: 'white'
  },
  slide5: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    backgroundColor: 'white'
  },
   heading: {
     fontSize: 22,
     fontWeight: 'bold',
     paddingVertical: 10,
     textAlign: 'center',
     backgroundColor: 'white'
   },
  heading2: {
    fontSize: 23,
    fontWeight: 'bold',
    paddingVertical: 10,
    textAlign: 'center',
    backgroundColor: 'white'
  },
})

const Tab = createNativeStackNavigator();

export default function App () {
return(
    <NavigationContainer>
    <Tab.Navigator>
<Tab.Screen name= 'first' component={firstScreen} options={{headerShown: false}} />
<Tab.Screen name= 'second' component={secondScreen} options={{headerShown: false}} />
</Tab.Navigator>
</NavigationContainer>
  );
}