import * as React from 'react';
import { View, Text } from 'react-native';


export default function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: 'purple' }}>
      <Text
        onPress={() => alert('Completed Tasks Go Here')}
        style={{ fontSize: 26, fontWeight: 'bold' }}>Completed Tasks Go Here</Text>
    </View>
  );
}
